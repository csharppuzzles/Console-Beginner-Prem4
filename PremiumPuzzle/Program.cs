﻿
static void ExampleA()
{
    Console.WriteLine("\nExample A");
    Console.WriteLine("~~~~~~~~~");

    // Config Section

    Console.WriteLine("Tax Department Questionnaire");

    Console.WriteLine("What was your income for the tax year? :");
    decimal income = Convert.ToDecimal(Console.ReadLine());
    Console.WriteLine($"Recorded Answer: {income}");

    Console.WriteLine("How much do you have left?: ");
    decimal remaining = Convert.ToDecimal(Console.ReadLine());
    Console.WriteLine($"Recorded Answer: {remaining}");

    if(income > 0 && income <= 12000)
    {
        Console.WriteLine("You are in the 10% tax bracket");
    }
    else if (income > 12000 && income <= 50000)
    {
        Console.WriteLine("You are in the 12% tax bracket");
    }
    else if (income > 50000 && income <= 100000)
    {
        Console.WriteLine("You are in the 22% tax bracket");
    }
    else if(income > 100000)
    {
        Console.WriteLine("You are in the 30% tax bracket");
    }
    else
    {
        Console.WriteLine("Don't lie to the tax department!");
    }

    if (remaining > 0)
    {

        Console.WriteLine($"You have ${remaining} left? Send it all to us!");
    }
    else
    {
        Console.WriteLine("Probably, you owe us money then...");
    }

}


// Puzzle A - Magic Numbers
//
// Example A has "magic numbers".
// Numbers that may change at a later date but are hard coded into that program.
// Copy the program above and fix it so that the configuration section at the top contains variables for the hardcoded values.
/*
static void PuzzleA()
{
    Console.WriteLine("\nPuzzle A");
    Console.WriteLine("~~~~~~~~~");
}
*/

static void ExampleB()
{
    Console.WriteLine("\nExample B");
    Console.WriteLine("~~~~~~~~~");

    // Config section, avoid using "Magic Numbers"
    string companyName = "Lickin' Chicken";
    string catchphrase = "Don't be a chicken, try our fried chicken!";
    decimal dirtyBirdBox = 19.99m;
    decimal pigeonBurger = 14.99m;

    Console.WriteLine($"Welcome to {companyName}");
    Console.WriteLine($"Hey friend - {catchphrase}");

    Console.WriteLine("\n Would you like to place an order?");
    Console.WriteLine("1: Dirty Bird Box");
    Console.WriteLine("2: Pigeon Burger, we mean Chicken Burger");

    Console.WriteLine("Please enter the number of your selection: ");
    int choice = Convert.ToInt32(Console.ReadLine());

    decimal totalCost = 0m;
    if (choice == 1)
    {
        Console.WriteLine("Great! Let me just throw it in the microwave and it'll be ready in a couple minutes!");
        totalCost = totalCost + dirtyBirdBox;
    }
    else if (choice == 2)
    {
        Console.WriteLine("Excellent! I'll go shoot a pigeon, I mean chicken, it won't take long...");
        totalCost = totalCost + pigeonBurger;
    }
    else
    {
        Console.WriteLine("Error. Enter '1' or '2' or get out.");
    }

    Console.WriteLine($"Cost: {totalCost}");

}


// Puzzle B - Dirty Bird
// 
// Copy the example "Lickin' Chicken" program above.
// Change the catchphrase and increase price of the menu items due to inflation.
// Add in a 3rd option (of some chicken-related snack).
// When the user makes their selection, ask them
//  "Do you want to SUPER-SIZE it for an extra $2? Enter 'y' / 'n':"
// Add 2.00 onto the total (or not)
/*
static void PuzzleB()
{
    Console.WriteLine("\nPuzzle B");
    Console.WriteLine("~~~~~~~~~");
}
*/


static void ExampleC()
{
    Console.WriteLine("\nExample C");
    Console.WriteLine("~~~~~~~~~");


    Console.WriteLine("\nHey, You! Enter a four letter word: ");
    string? word = Console.ReadLine();

    if(word != null && word != "")
    {

        if(word.Length != 4)
        {
            Console.WriteLine("Error! I said enter a FOUR letter word");
        } else
        {
            Console.WriteLine($"Good job! Your word was \"{word}\" and that has 4 letters.");
        }

    } else
    {
        Console.WriteLine("Error! I said enter a word");
    }


}


// Puzzle C - Common Letters
//
// Write a program that asks the user to enter a word
// If the word ends in the letter "e", "s" "d" or "t" print
//  "The most common letter a word ends in are 'e', 's', 'd' and 't'"
//  "Your word {word} ends in a {lastLetter}"
// Otherwise, tell them "Nice word...{word}"
// Learn about "e" double quotes, single quotes 'e'
// Learn how to get the last letter of a word
// Learn multiple ORs inside an if statement
/*
static void PuzzleC()
{
    Console.WriteLine("\nPuzzle C");
    Console.WriteLine("~~~~~~~~~");
}
*/


// Run the puzzles

ExampleA();
//PuzzleA();

ExampleB();
//PuzzleB();

ExampleC();
//PuzzleC();



Console.WriteLine("\n Press enter to exit the program...");
Console.ReadLine();                                         // Keeps the console app window open until you hit enter